Contains example implementations of generative adversarial networks. Includes toy examples and extensions to different distributions such as Poisson.

